# Semantic Versioning Release CI Jobs

CI Jobs for:
- Merging `master` branch to a `stable` branch
    - Ideally done automatically after tests have run
    - Can be done manually if tests are not exhaustive enough
- Creates a new tag based on Semantic Versioning
    - Job is manual, click on `Release Major`, `Release Minor`, `Release Patch` buttons to trigger the CI
    - The major/minor/patch version is automatically incremented depending on the triggered job
- The tags are only created on the `stable` branch

Requirements:
- A `stable` branch must exist
- Variable `GITLAB_DEPLOY_KEY` must be set
    - It should be a `base64` encoded SSH key with read and write access
- A semver tag should already exist (Just create a `0.0.0` tag manually and push it)

Include via:
```yaml
include: 'https://gitlab.com/davidevi/ddv-ci/raw/master/semver-release/.semver-release.yml'
```

Issues:
- `Error loading key "key": invalid format` - Your key is either not in base64,
or its missing a newline at the end of it (before encoding it)

Future improvements:
- Automatically create `stable` branch
    - Or at least notify user if it does not exist
- Notify user if GITLAB_DEPLOY_KEY is not set properly, not a valid SSH key,
 or has a newline at the end
- Remove requirement for having an initial manually-created semver tag already
existing
- Should not actually merge nor create tags if it's in a `*-ci` branch
- If last tag was not semver-compatible, CI will break
