# DDV Continuous Integration

Collection of GitLab CI configurations and Dockerfiles that could
be used for CI pipelines.

## GitLab CI How-To

For complete instructions, see the GitLab guide for how to include other
configurations in your CI: [GitLab CI/CD Include Examples](https://docs.gitlab.com/ee/ci/yaml/includes.html#single-string-or-array-of-multiple-values)

Here is a quick example:
```yaml
include: 'https://gitlab.com/awesome-project/raw/master/.before-script-template.yml'
```
