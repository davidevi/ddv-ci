# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2020-12-28
- Add `docker` binary to `python-poetry-ci` and `alpine-ci`

## [0.2.0] - 2020-10-25
- Create `alpine-ci` docker image for an alpine image with necessary tools for CI pipelines
- Create `python-poetry` docker image for an image with a Python base and Poetry installed
- Create `python-poetry-ci` docker image for an image with python, poetry, and necessary tools for CI pipelines

## [0.1.0] - 2020-10-25
- Create `semver-release` CI configuration for convenient pipeline-based release
process
