# Alpine CI

Alpine Docker image with:
- `git`
- `openssh-client`
- `curl`
- `make`
- `bash`
- `docker`

The image tag follows the underlying Alpine version.
