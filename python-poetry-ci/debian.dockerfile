ARG DEBIAN_VERSION
ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-${DEBIAN_VERSION}

RUN apt update && \
    apt install -y \
    git \
    openssh-client \
    curl \
    make \
    coreutils \
    bash \
    lsb-release

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null  && \
    apt-get update  && \
    apt-get install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io 
 
RUN pip install poetry flake8 black
