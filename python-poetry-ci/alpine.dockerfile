ARG ALPINE_VERSION
ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

RUN apk add --update \
    git \
    openssh-client \
    curl \
    make \
    coreutils \
    bash \
    docker

RUN apk add --no-cache --virtual .build-deps gcc musl-dev \
	libffi-dev openssl-dev python3-dev

RUN pip install poetry flake8 black
