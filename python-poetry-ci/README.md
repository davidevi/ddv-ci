# Python Poetry CI

Python image with:
- [Poetry](https://python-poetry.org/)
- [Flake8](https://flake8.pycqa.org/en/latest/)
- [Black](https://pypi.org/project/black/)
- [Invoke](http://www.pyinvoke.org/)
- `git`
- `openssh-client`
- `curl`
- `make`
- `bash`
- `docker`

Tag follows the base Python image.
